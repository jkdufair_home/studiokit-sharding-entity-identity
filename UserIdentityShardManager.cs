﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Extensions;
using StudioKit.Sharding.Models;
using StudioKit.Sharding.Utils;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Windows.Forms;

namespace StudioKit.Sharding.Entity.Identity
{
	public class UserIdentityShardManager<TContext, TConfiguration, TLicense, TUser, TIdentityProvider>
		: EntityShardManager<TContext, TConfiguration, TLicense>
		where TContext : UserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>
		where TConfiguration : BaseShardConfiguration, new()
		where TLicense : License, new()
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
		where TIdentityProvider : IdentityProvider, new()
	{
		public virtual string WebUrl => $"https://{Subdomain}.example.org";

		public virtual string EncodedWebUrl => HttpUtility.UrlEncode(WebUrl);

		public virtual string ApiUrl => $"https://{Subdomain}.api.example.org";

		public virtual string LogoutUrl => $"{ApiUrl}/api/Account/Logout";

		public virtual string ExternalLoginUrlFormat => $"{ApiUrl}/api/Account/ExternalLogin?provider={{0}}&returnUrl={EncodedWebUrl}";

		public virtual string MobileLoginUrl => $"{ApiUrl}/api/Account/RegisterExternal";

		public virtual string CasLoginUrl => $"https://www.purdue.edu/apps/account/cas/login?service={EncodedWebUrl}";

		public virtual string CasLogoutUrl => $"https://www.purdue.edu/apps/account/cas/logout?service={EncodedWebUrl}";

		public virtual string ShibbolethReturnPath => "/api/Account/Shibboleth";

		public virtual string ShibbolethLogoutUrl => $"{ApiUrl}/Shibboleth.sso/Logout";

		public override void SeedShardDatabase(ShardLocation location)
		{
			base.SeedShardDatabase(location);

			var context = GetDbContext(location.GetConnectionString());
			SeedIdentityProviders(context);
		}

		protected virtual void SeedIdentityProviders(TContext context)
		{
			var identityProviders = context.IdentityProviders.ToList();

			ConsoleUtils.WriteInfo("Current IdentityProviders: ");
			foreach (var identityProvider in identityProviders)
			{
				ConsoleUtils.WriteObjectProperties(identityProvider);
				Console.WriteLine();
			}

			if (!IsConsole) return;

			Console.Write("Add Shibboleth IdentityProvider Endpoint (or leave blank to skip): ");
			var response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response))
			{
				byte[] image = null;
				Console.Write("Add a Login Button Image? (Y/N or leave blank to skip): ");
				var imageResponse = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(imageResponse) && imageResponse.ToUpperInvariant().Equals("Y"))
				{
					var dialog = new OpenFileDialog
					{
						Multiselect = false,
						Title = "Select Login Button Image",
						Filter = "Image|*.png;*.gif;*.jpg;*.jpeg;*.bmp"
					};
					using (dialog)
					{
						if (dialog.ShowDialog() == DialogResult.OK)
						{
							var filename = dialog.FileName;
							image = File.ReadAllBytes(filename);
						}
					}
				}

				var entityId = HttpUtility.UrlEncode(response);
				var target = HttpUtility.UrlEncode($"{ShibbolethReturnPath}?shardKey={ShardKey}&returnUrl={EncodedWebUrl}");
				var mobileTarget = HttpUtility.UrlEncode($"{ShibbolethReturnPath}?shardKey={ShardKey}");
				var loginUrl = $"{ApiUrl}/Shibboleth.sso/Login?entityID={entityId}&target={target}";
				var mobileLoginUrl = $"{ApiUrl}/Shibboleth.sso/Login?entityID={entityId}&target={mobileTarget}";
				context.IdentityProviders.Add(
					new TIdentityProvider
					{
						Name = CustomerName,
						LoginUrl = loginUrl,
						LogoutUrl = $"{ApiUrl}{ShibbolethLogoutUrl}",
						MobileLoginUrl = mobileLoginUrl,
						Text = $"Log in with your {CustomerName} Account",
						Image = image
					});

				ConsoleUtils.WriteInfo("Added IdentityProvider \"{0}\" with URL \"{1}\"", CustomerName, loginUrl);
			}

			Console.Write("Add Purdue CAS IdentityProvider (Y/N or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
			{
				context.IdentityProviders.Add(
					new TIdentityProvider
					{
						Name = "Purdue CAS",
						LoginUrl = CasLoginUrl,
						LogoutUrl = CasLogoutUrl,
						MobileLoginUrl = CasLoginUrl,
						Text = "Log in with your Purdue Career Account"
					});

				ConsoleUtils.WriteInfo("Added Purdue CAS IdentityProvider");
			}

			Console.Write("Enable Google Login? (Y/N or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
			{
				context.IdentityProviders.Add(
					new TIdentityProvider
					{
						Name = "Google",
						LoginUrl = string.Format(ExternalLoginUrlFormat, "Google"),
						LogoutUrl = LogoutUrl,
						MobileLoginUrl = MobileLoginUrl,
						Text = "Log in with your Google Account"
					});

				ConsoleUtils.WriteInfo("Added Google IdentityProvider");
			}

			Console.Write("Enable Facebook Login? (Y/N or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
			{
				context.IdentityProviders.Add(
					new TIdentityProvider
					{
						Name = "Facebook",
						LoginUrl = string.Format(ExternalLoginUrlFormat, "Facebook"),
						LogoutUrl = LogoutUrl,
						MobileLoginUrl = MobileLoginUrl,
						Text = "Log in with your Facebook Account"
					});

				ConsoleUtils.WriteInfo("Added Facebook IdentityProvider");
			}

			if (context.ChangeTracker.HasChanges())
			{
				context.SaveChanges();

				identityProviders = context.IdentityProviders.ToList();
				ConsoleUtils.WriteInfo("Saved IdentityProviders: ");
				foreach (var identityProvider in identityProviders)
				{
					ConsoleUtils.WriteObjectProperties(identityProvider);
					Console.WriteLine();
				}
			}
			else
			{
				ConsoleUtils.WriteInfo("No changes were made.");
			}
			Console.WriteLine();
		}

		protected override void ConsoleEditLicense(TLicense license)
		{
			base.ConsoleEditLicense(license);

			var intResponse = 0;
			while (intResponse == 0)
			{
				intResponse = ConsoleUtils.ReadIntegerInput("Set License User Limit: ", 0, i => true);
				if (intResponse != 0)
				{
					license.UserLimit = intResponse;
				}
				else
				{
					ConsoleUtils.WriteWarning("You must enter a User Limit. Try again.");
				}
			}
		}
	}
}