using System.Data.Common;
using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Identity.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Entity.Identity
{
	public class UserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>
		: UserIdentityDbContext<TUser, TIdentityProvider>,
			IUserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>
		where TUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
		where TIdentityProvider : IdentityProvider, new()
		where TConfiguration : BaseShardConfiguration, new()
		where TLicense : License, new()
	{
		public UserIdentityShardDbContext() : this("DefaultConnection")
		{
		}

		public UserIdentityShardDbContext(DbConnection existingConnection) : base(existingConnection)
		{
		}

		public UserIdentityShardDbContext(string nameOrConnectionString)
			: base(nameOrConnectionString) { }

		public virtual DbSet<TConfiguration> Configurations { get; set; }

		public virtual DbSet<TLicense> Licenses { get; set; }

		public virtual DbSet<LicenseUser> LicenseUsers { get; set; }

		public TConfiguration GetConfiguration()
		{
			return Configurations.OrderByDescending(c => c.Id).FirstOrDefault();
		}

		public async Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return await Configurations.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
		}

		public TLicense GetLicense()
		{
			return Licenses.OrderByDescending(c => c.Id).FirstOrDefault();
		}

		public async Task<TLicense> GetLicenseAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return await Licenses.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
		}
	}
}