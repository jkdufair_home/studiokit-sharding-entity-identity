﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StudioKit.Data.Interfaces;

namespace StudioKit.Sharding.Entity.Identity.Models
{
	public class LicenseUser : IAuditable
	{
		[Key, Column(Order = 0)]
		public int LicenseId { get; set; }

		[ForeignKey("LicenseId")]
		public virtual License License { get; set; }

		[Key, Column(Order = 1)]
		public string UserId { get; set; }

		[ForeignKey("UserId")]
		public virtual IUser User { get; set; }

		public DateTime DateStored { get; set; }

		public DateTime DateLastUpdated { get; set; }

		public string LastUpdatedById { get; set; }
	}
}