﻿using StudioKit.Sharding.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Sharding.Entity.Identity.Models
{
	public class License : BaseLicense
	{
		public int UserLimit { get; set; }

		[InverseProperty("License")]
		public virtual ICollection<LicenseUser> LicenseUsers { get; set; }
	}
}